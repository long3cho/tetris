import java.net.*;
import java.io.*;
import java.util.*;

class Server
{
	private ServerSocket ss;
	private Socket socket;
	private Vector<Handler> v=new Vector<Handler>();
	private Handler handler;

	public Server() throws BindException
	{
		try
		{
			ss=new ServerSocket(9500);
			System.out.println("서버 준비 완료.");

			while(true)
			{
				socket = ss.accept();

				//스레드
				handler = new Handler(this,socket);
				//시작
				handler.start();
			}
		}
		catch(IOException io)
		{
			io.printStackTrace();
		}
	}
	public void register(Handler ch) throws IOException
	{
		if(v.size()!=5)
		{
			v.add(handler);
		}
		else
		{
			socket.close();
		}
	}
	public void unregister(Handler ch)
	{
		for(int i=0;i<v.size();i++)
		{
			v.remove(i);
		}
		v.remove(handler);
	}
	public void broadcast(String message)
	{
		Protocol protocol = new Protocol();
		
		for(Handler handler : v)
		{
			
			handler.getPw().println(message);
			handler.getPw().flush();
			
		}
	}
	public void gameState(int hp)
	{
		for(Handler handler : v)
		{
			handler.getHp();
		}
	}
	
	public void main(String args[]) throws BindException{
		
		new Server();
		
	}

}