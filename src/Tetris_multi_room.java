import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Tetris_multi_room extends JFrame implements ActionListener, Runnable, KeyListener{

	private Tetris_multi tetris1;
	private Tetris_multi tetris2;
	
	private BufferedReader br = null;
	private PrintWriter pw = null;
	private InputStream in;
	private OutputStream out;
	private Socket socket;
	private String nickName;
	
	final int frame_x_size = 1200;
	final int frame_y_size = 900;
	
	private JPanel game_panel;
	
	private JPanel chat_panel;
	private JTextArea chatlog;
	private JTextField chat;
	private JButton send;
	
	public Tetris_multi_room(Socket s, String name){
		
		
		setSize(frame_x_size, frame_y_size);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		Dimension d = getToolkit().getScreenSize();
		
		setLocation((int)d.getWidth()/2-(frame_x_size/2),(int)d.getHeight()/2-(frame_y_size/2));
		
		socket = s;
		nickName = name;
		
		chatlog = new JTextArea();
		chatlog.setEditable(false);
		
		chat = new JTextField();
		
		send = new JButton("메시지 전송");
		
		JScrollPane chatls = new JScrollPane(chatlog);
		chatls.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		JScrollPane chats = new JScrollPane(chat);
		chats.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		chat_panel= new JPanel();
		chat_panel.setLayout(null);
		chat_panel.add(chatls);
		chat_panel.add(chats);
		chat_panel.add(send);
		
		send.addActionListener(this);
		chat.addKeyListener(this);
		
		chatls.setBounds(32, 600, 736, 192);
		chats.setBounds(32, 792, 544, 32);
		send.setBounds(576, 792, 192, 32);
		
		add(chat_panel);
		tetris1 = new Tetris_multi();
		System.out.println(tetris1.toString());
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				JOptionPane jop = new JOptionPane();
				int comfirm = JOptionPane.showConfirmDialog(
						Tetris_multi_room.this, "종료하시겠습니까?", "종료",
						JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				// jop.showConfirmDialog(MSPaint.this,"종료하시겠습니까?","종료",jop.OK_CANCEL_OPTION,jop.QUESTION_MESSAGE);
				if (comfirm == JOptionPane.OK_OPTION) {
					try {
						if (socket != null) {
							pw.println("quit");
							pw.flush();

							br.close();
							pw.close();
							socket.close();
						}

						System.exit(0);
					} catch (IOException io) {
						System.out.println("서버와 연결이 끊어졌습니다");
					}
				} else if (comfirm == JOptionPane.CANCEL_OPTION) {
					setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				}
			}
		});
		
		
		try {
			br = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream())));
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (IOException io) {
			System.out.println("서버와의 연결이 끊어졌습니다");
			System.exit(0);
		}
		pw.println(nickName);
		pw.flush();

		Thread t = new Thread(this);
		t.start();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if(e.getSource() == send){
			
			if (chat.getText().length() != 0) {
				String data = chat.getText();
				pw.println(data);
				pw.flush();
				chat.setText("");
			}
			
		}

		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			if (chat.getText().length() != 0) {
				String data = chat.getText();
				pw.println(data);
				pw.flush();
				chat.setText("");
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void run() {
		
		String line = null;
		while (true) {
			try {
				line = br.readLine();

				if (line == null || line.toUpperCase().equals("quit")) {
					br.close();
					pw.close();
					socket.close();
				}
				chatlog.append(line + "\n");

				int pos = chatlog.getText().length();
				chatlog.setCaretPosition(pos);
			} catch (IOException e) {
				System.out.println("서버와의 연결이 끊어졌습니다");
				System.exit(0);
			}
		}
		
	}

}
