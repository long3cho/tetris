import java.net.*;
import java.io.*;

class Handler extends Thread 
{
	private Server s;
	private Socket socket;
	public BufferedReader br;
	public PrintWriter pw;
	public InputStream in;
	public OutputStream out;

	public Handler(Server cs, Socket socket)
	{
		this.s=cs;
		this.socket=socket;
		
		try
		{
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
			in = socket.getInputStream();
			out = socket.getOutputStream();
		}
		catch(IOException io)
		{
			io.printStackTrace();
		}
	}
	public void run()
	{
		String nickName=null;
		try
		{
			nickName = br.readLine();
		}
		catch(IOException io)
		{
		}
		//���Ϳ� ����
		try
		{
			s.register(this);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		s.broadcast(nickName+"���� �����Ͽ����ϴ�.");
		
		String line = null;

		while(true)
		{
			Protocol protocol = new Protocol();
			try
			{
				line = br.readLine();
			}
			catch(IOException io)
			{
				io.printStackTrace();
			}
			if(line==null || line.toUpperCase().equals("QUIT")) 
			{
				break;
			}
			
			s.broadcast("["+nickName+"]"+line);
				
		}

		s.unregister(this);
		s.broadcast(nickName+"���� �����ϼ̽��ϴ�.");
		
		try
		{
			br.close();
			pw.close();
			socket.close();
		}
		catch(IOException io)
		{
			System.out.println("����� IO�� ���� ���߽��ϴ�");
		}
	}

	public PrintWriter getPw()
	{
		return pw;
	}
	public OutputStream getHp()
	{
		return out;
	}
}