import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Tetris_single extends JFrame implements ActionListener, Runnable{
	
	Thread clock;
	
	Image off; //메모리상 가상 화면
	Graphics offG;
	
	Random r;
	
	byte[][] map;
	Color[] colorType;
	Color[][] colorMap;
	
	int blockType; //블럭의 종류
	int[] blockX; //블럭의 좌표
	int[] blockY; //블럭의 좌표
	//-------------------------------------------------------------
	int[] ghostX; //고스트 블럭의 좌표
	int[] ghostY; //고스틔 블럭의 좌표
	int[] tmpX; //고스트 블럭을 위한 임시 저장 좌표
	int[] tmpY; //고스트 블럭을 위한 임시 저장 좌표
	//-------------------------------------------------------------
	int blockPos;
	int ghostBlockPos;
	
	int score;
	int delayTime;
	int runGame; //1이면 게임 시작, 2이면 게임 종료
	
	final int game_width = 300;
	final int game_height = 525;
	
	boolean debug;
	
	JButton startButton;
	JPanel buttonPanel;
	
	public Tetris_single(){
		
		setSize(500, 700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		
		Dimension d = getToolkit().getScreenSize();
		
		setLocation((int)d.getWidth()/2-(this.getWidth()/2),(int)d.getHeight()/2-(this.getHeight()/2));
		
		//메모리상에 가상 화면 만들기
		off = createImage(300, 525);
		offG = off.getGraphics();
		offG.setColor(Color.white);
		offG.fillRect(0, 0, game_width, game_height);
		
		setLayout(new BorderLayout());
		buttonPanel = new JPanel();
		
		startButton = new JButton("게임 시작");
		startButton.addActionListener(this);
		buttonPanel.add(startButton);
		add(BorderLayout.SOUTH, buttonPanel);
		
		map = new byte[12][21];
		colorMap = new Color[12][21];
		colorType = new Color[7];
		setColorType();
		
		blockX = new int[4];
		blockY = new int[4];
		//--------------------------------------------------고스트 블럭 초기화-----------------
		ghostX = new int[4];
		ghostY = new int[4];
		tmpX = new int[4];
		tmpY = new int[4];
		for(int i = 0; i < 4; i++){
			
			ghostX[i] = ghostY[i] = 0;
			
		}
		//---------------------------------------------------------
		blockPos = 0;
		ghostBlockPos = 0;
		
		r = new Random();
		blockType = Math.abs(r.nextInt() % 7); // 랜덤하게 블럭 결정
		
		setBlockXY(blockType);
		
		drawBlock();
		drawMap();
		
		score = 0;
		delayTime = 1000; //1초
		runGame = 0;
		

		addKeyListener(new MyKeyHandler());
		
		if(clock == null){
			clock = new Thread(this);
			clock.start();
		}
		
		
	}
	

	private void setColorType() {

		colorType[0] = Color.ORANGE;
		colorType[1] = Color.CYAN;
		colorType[2] = Color.GREEN;
		colorType[3] = Color.RED;
		colorType[4] = Color.YELLOW;
		colorType[5] = Color.MAGENTA;
		colorType[6] = Color.PINK;			
		
	}


	public void paint(Graphics g){
		//가상 화면을 실제 화면에 출력
		g.drawImage(off, (this.getWidth()-game_width)/2, (this.getHeight()-game_height)/2, this);
	}
	
	public void update(Graphics g){
		paint(g);
	}
	
	public void setBlockXY(int type){
		//블록 모양 결정
		/* (a) oo (b) o  (c)   o  (d) o   (e) o  (f)  o (g) oooo                                                                  
		       oo    ooo     ooo      ooo     oo     oo                                                   
		       								   o	 o			 */
		switch(type){
		case 0: //(a)									
			blockX[0] = 5; blockY[0] = 0;
			blockX[1] = 6; blockY[1] = 0;  
			blockX[2] = 5; blockY[2] = 1;    
			blockX[3] = 6; blockY[3] = 1;
			break;
		case 1: //(b)
			blockX[0] = 6; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 1;  
			blockX[2] = 6; blockY[2] = 1;    
			blockX[3] = 7; blockY[3] = 1;
			break;
		case 2: //(c)
			blockX[0] = 7; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 1;  
			blockX[2] = 6; blockY[2] = 1;    
			blockX[3] = 7; blockY[3] = 1;
			break;
		case 3: //(d)
			blockX[0] = 5; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 1;  
			blockX[2] = 6; blockY[2] = 1;    
			blockX[3] = 7; blockY[3] = 1;
			break;
		case 4: //(e)
			blockX[0] = 5; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 1;  
			blockX[2] = 6; blockY[2] = 1;    
			blockX[3] = 6; blockY[3] = 2;
			break;
		case 5: //(f)
			blockX[0] = 6; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 1;  
			blockX[2] = 6; blockY[2] = 1;    
			blockX[3] = 5; blockY[3] = 2;
			break;
		case 6: //(g)
			blockX[0] = 4; blockY[0] = 0;
			blockX[1] = 5; blockY[1] = 0;  
			blockX[2] = 6; blockY[2] = 0;    
			blockX[3] = 7; blockY[3] = 0;
			break;
		}
		//----------------------------------------------------------------고스트블록에 복사------
		tmpX[0] = blockX[0]; tmpY[0] = blockY[0];
		tmpX[1] = blockX[1]; tmpY[1] = blockY[1];
		tmpX[2] = blockX[2]; tmpY[2] = blockY[2];
		tmpX[3] = blockX[3]; tmpY[3] = blockY[3];
		//------------------------------------------------------------------------
	}
	
	public void drawTitle() {
		offG.setColor(Color.blue);
		offG.drawString("Tetris Game ", game_width/2-30, game_height/2-100);
	}
 
	public void drawScore() {
		offG.setColor(Color.red);
		offG.drawString("Game Over! ", game_width/2-30, game_height/2-150);
		offG.setColor(Color.red);
		offG.drawString("Score: "+score, game_width/2-30, game_height/2-100);
	}
 
	public void dropBlock() {
		removeBlock();
		
		if(checkDrop()){
			for(int i=0; i<4; i++){
				blockY[i] = blockY[i]+1;
			}
		}
		else{
			drawBlock();
			nextBlock();
		}
	}
 
	//줄 지우기
	public void delLine(){
		boolean delOK;
		
		for(int row=20; row>=0; row--){
			delOK = true;
			for(int col=0; col<12; col++){
				if(map[col][row] == 0) delOK = false;
			}
			
			if(delOK){
				score+=10;
			
			
				for(int delRow=row; delRow>0; delRow--){
					for(int delCol=0; delCol<12; delCol++){
						map[delCol][delRow] = map[delCol][delRow-1];
						colorMap[delCol][delRow] = colorMap[delCol][delRow-1];
					}
				}
				
				for(int i=0; i<12; i++){
					map[0][i] = 0;
				}
				
				row++;
			}
		}
	}
	
	public void nextBlock() {
		blockType = Math.abs(r.nextInt() % 7);
		blockPos = 0;
		ghostBlockPos = 0;
		delLine();
		setBlockXY(blockType);
		checkGameOver();
	}
 
	public void checkGameOver() {
		for(int i=0; i<4; i++){
			if(map[blockX[i]][blockY[i]] == 1){
				if(runGame == 1)
					runGame = 2;
			}
		}
	}
 
	public void removeBlock() {
		for(int i=0; i<4; i++){
			map[blockX[i]][blockY[i]] = 0;
			colorMap[blockX[i]][blockY[i]] = Color.WHITE;
		}
	}
	
	public boolean checkDrop() {
		boolean dropOk = true;
		
		for(int i=0; i<4; i++){
			if((blockY[i]+1)!=21){
				if(map[blockX[i]][blockY[i]+1] == 1) dropOk = false;
 			}
			else
				dropOk = false;
		}
		return dropOk;
	}
 
	//--------------------------------------------------고스트블록 높이 구하기----------------------
	public int getMax(){
		int max = 0;
		
		for(int i=0; i<4; i++){
			if(tmpY[i]>max)
				max = tmpY[i];
		}
		return max;
	}
	//-------------------------------------------------------------------------------------------
	public void drawBlock() { //debug
		//--------------------------------------------------------------블록, 고스트블록 그리기----------
		boolean check = false;
		for(int i=0; i<4; i++){			
			map[ghostX[i]][ghostY[i]] = 0;			
		}
		
		for(int j=20-getMax(); j>=0; j--){
			for(int i=0; i<4; i++){
				if(map[tmpX[i]][j+tmpY[i]] == 1)
					break;
				if(i==3){
					check = true;
				}
			}
			if(check == true){
				if(checkDrop()){
					for(int i=0; i<4; i++){
						ghostX[i] = tmpX[i];
						ghostY[i] = j+tmpY[i];
						map[ghostX[i]][ghostY[i]] = 2;
					}
				}
				else{
					for(int i=0; i<4; i++){
						map[ghostX[i]][ghostY[i]] = 0;
						ghostX[i] = ghostY[i] = 0;
					}
				}				
				break;
			}
		}
		
		for(int i=0; i<4; i++){
			map[blockX[i]][blockY[i]] = 1;	
			colorMap[blockX[i]][blockY[i]] = colorType[blockType];
		}
		
		//---------------------------------------------------------------------------------------
	}
	
	public void drawMap() {
		for(int i=0; i<12; i++){
			for(int j=0; j<21; j++){
				if(map[i][j] == 2){
					offG.setColor(Color.BLACK);
					offG.clearRect(i*25, j*25, 25, 25);
					
				}
				else if(map[i][j] == 1){
					
					offG.setColor(colorMap[i][j]);
					offG.fillRect(i*25, j*25, 25, 25);
					offG.setColor(Color.BLACK);
					offG.drawRect(i*25, j*25, 25, 25);
				}else{
					offG.setColor(Color.white);
					offG.fillRect(i*25, j*25, 25, 25);
				}
			}
		}
	}
	
class MyKeyHandler extends KeyAdapter {
		
		public void keyPressed(KeyEvent e){
			
			int keyCode = (int)e.getKeyCode();
			
			
			if(runGame == 1){
				//왼쪽 키
				if(keyCode==KeyEvent.VK_LEFT){
					if(checkMove(-1)){
						for(int i=0; i<4; i++){
							blockX[i] = blockX[i]-1;
							//--------------------------------------------------고스트블록 이동----------------------
							tmpX[i] -= 1;
							//----------------------------------------------------------------------
						}
					}
				}
				
				//오른쪽 키
				if(keyCode==KeyEvent.VK_RIGHT){
					if(checkMove(1)){
						for(int i=0; i<4; i++){
							blockX[i] = blockX[i]+1;
							//-----------------------------------------------------고스트블록 이동-------------------
							tmpX[i] += 1;
							//------------------------------------------------------------------------
						}
					}
				}
				
				//아래쪽 키
				if(keyCode==KeyEvent.VK_DOWN){
					
					removeBlock();
					
					if(checkDrop()){
						for(int i=0; i<4; i++)
							blockY[i] = blockY[i]+1;
					}
					else{
						drawBlock();
					}	
				}
				
				//위쪽 키
				if(keyCode==KeyEvent.VK_UP){
					int[] tempX = new int[4];
					int[] tempY = new int[4];
					int[] gtempX = new int[4];
					int[] gtempY = new int[4];
					
					for(int i=0; i<4; i++){
						tempX[i] = blockX[i];
						tempY[i] = blockY[i];
						gtempX[i] = tmpX[i];
						gtempY[i] = tmpY[i];
						
					}
					
					removeBlock();
					//--------------------------------고스트블록 회전-------------------
					turnGhostBlock();
					//---------------------------------------------------
					
					if(checkGhostTurn()){
						if(ghostBlockPos<4)
							ghostBlockPos++;
						else
							ghostBlockPos = 0;
					}
					else{
						for(int i=0; i<4; i++){
							
							tmpX[i] = gtempX[i];
							tmpY[i] = gtempY[i];
					
						}
					}
					
					turnBlock();
					
					if(checkTurn()){
						if(blockPos<4)
							blockPos++;
						else
							blockPos = 0;
					}
					else{
						for(int i=0; i<4; i++){
							
							blockX[i] = tempX[i];
							blockY[i] = tempY[i];
							map[blockX[i]][blockY[i]] = 1;
							colorMap[blockX[i]][blockY[i]] = colorType[blockType];
						}
					}
					
				}
				
				//스페이스바
				if(keyCode == KeyEvent.VK_SPACE){
					
					removeBlock();
					
					while(checkDrop()){
						
						if(checkDrop()){
							for(int i=0; i<4; i++)
								blockY[i] = blockY[i]+1;
									
						}
						else{
							
							drawBlock();
						}	
					}

				}
				
				
			drawBlock();
			drawMap();
			repaint();
			
			}
				
		}
		
		//debug
		public boolean checkTurn() {
			boolean turnOk = true;
			
			for(int i=0; i<4; i++){
				if((blockX[i]>=0)&&(blockX[i]<12)&&(blockY[i]>=0)&&(blockY[i]<21)){
					if(map[blockX[i]][blockY[i]] == 1) turnOk = false;
				}
				else
					
					turnOk = false;
			}
			
			return turnOk;
		}
		
		public boolean checkGhostTurn() {
			boolean turnOk = true;
			
			for(int i=0; i<4; i++){
				if((tmpX[i]>=0)&&(tmpX[i]<12)&&(tmpY[i]>=0)&&(tmpY[i]<21)){
					
				}
				else
					
					turnOk = false;
			}
			
			return turnOk;
		}
 
		public boolean checkMove(int dir) {
			boolean moveOk = true;
			
			removeBlock();
			
			for(int i=0; i<4; i++){
				if(((blockX[i]+dir)>=0)&&((blockX[i]+dir)<12)){
					if(map[blockX[i]+dir][blockY[i]] == 1)
						moveOk = false;
				}
				else
					moveOk = false;
			}
			
			if(!moveOk)
				drawBlock();
			
			
			return moveOk;
		}
		
		
		public void turnBlock() {
			switch(blockType){
			case 1:
				switch(blockPos){
				case 0:
					blockX[0]=blockX[0]; blockY[0]=blockY[0];
					blockX[1]=blockX[1]; blockY[1]=blockY[1];
					blockX[2]=blockX[2]; blockY[2]=blockY[2];
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]+1;
					break;
				case 1:
					blockX[0]=blockX[0]-1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]+1; blockY[2]=blockY[2]-1;
					blockX[3]=blockX[3]; blockY[3]=blockY[3]-1;
					break;
				case 2:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2]+1;
					blockX[3]=blockX[3]; blockY[3]=blockY[3]+1;
					break;
				case 3:
					blockX[0]=blockX[0]; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1];
					blockX[2]=blockX[2]-1; blockY[2]=blockY[2];
					blockX[3]=blockX[3]+1; blockY[3]=blockY[3]-1;
					break;
				}
				break;
			case 2:
				switch(blockPos){
				case 0:
					blockX[0]=blockX[0]-2; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2];
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]+1;
					break;
				case 1:
					blockX[0]=blockX[0]; blockY[0]=blockY[0];
					blockX[1]=blockX[1]; blockY[1]=blockY[1];
					blockX[2]=blockX[2]+1; blockY[2]=blockY[2]-1;
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]-1;
					break;
				case 2:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]-1; blockY[2]=blockY[2]+2;
					blockX[3]=blockX[3]+2; blockY[3]=blockY[3]+1;
					break;
				case 3:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1];
					blockX[2]=blockX[2]; blockY[2]=blockY[2]-1;
					blockX[3]=blockX[3]; blockY[3]=blockY[3]-1;
					break;
				}
				break;
			case 3:
				switch(blockPos){
				case 0:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1];
					blockX[2]=blockX[2]-1; blockY[2]=blockY[2]+1;
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]+1;
					break;
				case 1:
					blockX[0]=blockX[0]-2; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]+1; blockY[2]=blockY[2]-2;
					blockX[3]=blockX[3]; blockY[3]=blockY[3]-1;
					break;
				case 2:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1];
					blockX[2]=blockX[2]-1; blockY[2]=blockY[2]+1;
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]+1;
					break;
				case 3:
					blockX[0]=blockX[0]; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]+1; blockY[2]=blockY[2];
					blockX[3]=blockX[3]+2; blockY[3]=blockY[3]-1;
					break;
				}
				break;
			case 4:
				switch(blockPos){
				case 0:
				case 2:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+2; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]-1; blockY[2]=blockY[2];
					blockX[3]=blockX[3]; blockY[3]=blockY[3]-1;
					break;
				case 1:
				case 3:
					blockX[0]=blockX[0]-1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-2; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]+1; blockY[2]=blockY[2];
					blockX[3]=blockX[3]; blockY[3]=blockY[3]+1;
					break;
				}
				break;
			case 5:
				switch(blockPos){
				case 0:
				case 2:
					blockX[0]=blockX[0]-1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2];
					blockX[3]=blockX[3]+2; blockY[3]=blockY[3]-1;
					break;
				case 1:
				case 3:
					blockX[0]=blockX[0]+1; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2];
					blockX[3]=blockX[3]-2; blockY[3]=blockY[3]+1;
					break;
				}
				break;
			case 6:
				switch(blockPos){
				case 0:
				case 2:
					blockX[0]=blockX[0]+2; blockY[0]=blockY[0];
					blockX[1]=blockX[1]+1; blockY[1]=blockY[1]+1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2]+2;
					blockX[3]=blockX[3]-1; blockY[3]=blockY[3]+3;
					break;
				case 1:
				case 3:
					blockX[0]=blockX[0]-2; blockY[0]=blockY[0];
					blockX[1]=blockX[1]-1; blockY[1]=blockY[1]-1;
					blockX[2]=blockX[2]; blockY[2]=blockY[2]-2;
					blockX[3]=blockX[3]+1; blockY[3]=blockY[3]-3;
					break;
				}
				break;
			}

		}
		//----------------------------------------------고스트블록 회전 메소드------------------------------------------
		public void turnGhostBlock() {
			switch(blockType){
			case 1:
				switch(blockPos){
				case 0:
					tmpX[0]=tmpX[0]; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]+1;
					break;
				case 1:
					tmpX[0]=tmpX[0]-1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]+1; tmpY[2]=tmpY[2]-1;
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]-1;
					break;
				case 2:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2]+1;
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]+1;
					break;
				case 3:
					tmpX[0]=tmpX[0]; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]-1; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]+1; tmpY[3]=tmpY[3]-1;
					break;
				}
				break;
			case 2:
				switch(blockPos){
				case 0:
					tmpX[0]=tmpX[0]-2; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]+1;
					break;
				case 1:
					tmpX[0]=tmpX[0]; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]+1; tmpY[2]=tmpY[2]-1;
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]-1;
					break;
				case 2:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]-1; tmpY[2]=tmpY[2]+2;
					tmpX[3]=tmpX[3]+2; tmpY[3]=tmpY[3]+1;
					break;
				case 3:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2]-1;
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]-1;
					break;
				}
				break;
			case 3:
				switch(blockPos){
				case 0:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]-1; tmpY[2]=tmpY[2]+1;
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]+1;
					break;
				case 1:
					tmpX[0]=tmpX[0]-2; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]+1; tmpY[2]=tmpY[2]-2;
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]-1;
					break;
				case 2:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1];
					tmpX[2]=tmpX[2]-1; tmpY[2]=tmpY[2]+1;
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]+1;
					break;
				case 3:
					tmpX[0]=tmpX[0]; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]+1; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]+2; tmpY[3]=tmpY[3]-1;
					break;
				}
				break;
			case 4:
				switch(blockPos){
				case 0:
				case 2:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+2; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]-1; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]-1;
					break;
				case 1:
				case 3:
					tmpX[0]=tmpX[0]-1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-2; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]+1; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]; tmpY[3]=tmpY[3]+1;
					break;
				}
				break;
			case 5:
				switch(blockPos){
				case 0:
				case 2:
					tmpX[0]=tmpX[0]-1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]+2; tmpY[3]=tmpY[3]-1;
					break;
				case 1:
				case 3:
					tmpX[0]=tmpX[0]+1; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2];
					tmpX[3]=tmpX[3]-2; tmpY[3]=tmpY[3]+1;
					break;
				}
				break;
			case 6:
				switch(blockPos){
				case 0:
				case 2:
					tmpX[0]=tmpX[0]+2; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]+1; tmpY[1]=tmpY[1]+1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2]+2;
					tmpX[3]=tmpX[3]-1; tmpY[3]=tmpY[3]+3;
					break;
				case 1:
				case 3:
					tmpX[0]=tmpX[0]-2; tmpY[0]=tmpY[0];
					tmpX[1]=tmpX[1]-1; tmpY[1]=tmpY[1]-1;
					tmpX[2]=tmpX[2]; tmpY[2]=tmpY[2]-2;
					tmpX[3]=tmpX[3]+1; tmpY[3]=tmpY[3]-3;
					break;
				}
				break;
			}
			
		}
		//----------------------------------------------------------------------------------------------------
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		blockPos = 0;
		
		for(int i=0; i<12; i++){
			for(int j=0; j<21; j++){
				map[i][j] = 0;
			}
		}
		
		r = new Random();
		blockType = Math.abs(r.nextInt() % 7);
		setBlockXY(blockType);
		
		drawBlock();
		drawMap();
		
		score = 0;
		delayTime = 1000;
		runGame = 1;
		
		this.requestFocus();
	}


	
	@Override
	public void run() {
		
		while(true){
			try{
				clock.sleep(delayTime); //delayTime만큼 정지
			}catch(InterruptedException ie){}
			
			dropBlock();
			
			switch(runGame){
			default:
				drawTitle();
				break;
			case 1:
				drawBlock();
				drawMap();
				break;
			case 2:
				drawScore();
				break;
			}
			
			repaint(); //paint() 호출
		}
		
	}
	
}
