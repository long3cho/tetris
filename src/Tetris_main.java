
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


class Tetris_multi_frame extends JFrame implements MouseListener, Runnable {
	
	final int frame_x_size = 500;
	final int frame_y_size = 800;
	
	private JPanel main_panel = new JPanel();
	private JLabel server_on_label = new JLabel("서버 생성");
	private JLabel join_label = new JLabel("참가");
	private JLabel exit_label = new JLabel("취소");
	
	private Socket socket = null;
	private Thread server_thread;
	private Thread join_thread;
	
	
	public Tetris_multi_frame() throws BindException{
		
		setLayout(new BorderLayout());
		
		add(main_panel);
		main_panel.setLayout(null);
		main_panel.add(server_on_label);
		main_panel.add(join_label);
		main_panel.add(exit_label);
		server_on_label.addMouseListener(this);
		join_label.addMouseListener(this);
		exit_label.addMouseListener(this);
		server_on_label.setBounds(frame_x_size/2-50, frame_y_size/2+150, 100, 25);
		server_on_label.setFont(new Font("",0,20));
		join_label.setBounds(frame_x_size/2-50, frame_y_size/2+200, 60, 25);
		join_label.setFont(new Font("",0,20));
		exit_label.setBounds(frame_x_size/2-50, frame_y_size/2+250, 60, 25);
		exit_label.setFont(new Font("",0,20));
		
		Dimension d = getToolkit().getScreenSize();
		
		setLocation((int)d.getWidth()/2-(frame_x_size/2),(int)d.getHeight()/2-(frame_y_size/2));
		
		setSize(frame_x_size,frame_y_size);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	}

	@Override
	public void mouseClicked (MouseEvent e) {
		
		if(e.getSource() == server_on_label){
			
			Server_thread sh = new Server_thread();
			sh.start();
				
		}
		
		if(e.getSource() == join_label){
		
			String serverIP = JOptionPane.showInputDialog(this,"서버IP 입력","서버IP",JOptionPane.INFORMATION_MESSAGE);
			String nickName = JOptionPane.showInputDialog(this, "대화명 입력", "대화명 입력",
					JOptionPane.INFORMATION_MESSAGE);
			
			try {
				socket = new Socket(serverIP, 9500);
				new Tetris_multi_room(socket, nickName);
			} catch (UnknownHostException uhe) {
				System.out.println("서버를 찾을 수 없습니다.");
				System.exit(0);
			} catch (IOException io) {
				System.out.println("서버와 연결할 수 없습니다.");
				System.exit(0);
			}
			
			
		}
		
		if(e.getSource() == exit_label){
			
			this.dispose();
			
		}
		
	}
	
	class Server_thread extends Thread{
		
		
		public void run(){

			try {
				Server s = new Server();
			} catch (BindException e) {
				// TODO Auto-generated catch block

				System.out.println("서버가 이미 가동 중입니다.");
				
			}

		}
		
	}
	
	class join_thread extends Thread{
		
		public void run(){
			
			
			
		}
		
		
	}



	@Override
	public void mouseEntered(MouseEvent e) {
		
		if(e.getSource() == server_on_label)
			server_on_label.setFont(new Font("",Font.BOLD,20));
		
		if(e.getSource() == join_label)
			join_label.setFont(new Font("",Font.BOLD,20));
		
		if(e.getSource() == exit_label)
			exit_label.setFont(new Font("",Font.BOLD,20));
		
	}



	@Override
	public void mouseExited(MouseEvent e) {
		
setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		
		if(e.getSource() == server_on_label)
			server_on_label.setFont(new Font("",Font.PLAIN,20));
		
		if(e.getSource() == join_label)
			join_label.setFont(new Font("",Font.PLAIN,20));
		
		if(e.getSource() == exit_label)
			exit_label.setFont(new Font("",Font.PLAIN,20));
		
	}



	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void run() {

			
		
	}
	
	
}

public class Tetris_main extends JFrame implements MouseListener, Runnable {

	final int frame_x_size = 500;
	final int frame_y_size = 800;
	
	private JPanel main_panel = new JPanel();
	private JLabel single_label = new JLabel("Single");
	private JLabel multi_label = new JLabel("Multi");
	private JLabel exit_label = new JLabel("Exit");
	private JTextField id_field = new JTextField(15);
	private JPasswordField password_field = new JPasswordField(15);
	
	BufferedImage img = null;
	
	public Tetris_main() {
	
		try {
			
			img = ImageIO.read(new File("main.jpg"));
			
		} catch (IOException e) {
			
			System.out.println("No Image");
			System.exit(1);
			
		}
		
		setLayout(new BorderLayout());
		
		add(main_panel,BorderLayout.CENTER);
		
		main_panel.setLayout(null);
		main_panel.add(single_label);
		main_panel.add(multi_label);
		main_panel.add(exit_label);
		single_label.addMouseListener(this);
		multi_label.addMouseListener(this);
		exit_label.addMouseListener(this);
		single_label.setBounds(frame_x_size/2-50, frame_y_size/2+150, 60, 25);
		single_label.setFont(new Font("",0,20));
		multi_label.setBounds(frame_x_size/2-50, frame_y_size/2+200, 60, 25);
		multi_label.setFont(new Font("",0,20));
		exit_label.setBounds(frame_x_size/2-50, frame_y_size/2+250, 60, 25);
		exit_label.setFont(new Font("",0,20));
		
		Dimension d = getToolkit().getScreenSize();
		
		setLocation((int)d.getWidth()/2-(frame_x_size/2),(int)d.getHeight()/2-(frame_y_size/2));
		
		setSize(frame_x_size,frame_y_size);
		setVisible(true);
		setTitle("Tetris");
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		if(e.getSource() == single_label){
			
			new Tetris_single();
			this.dispose();
			
		}
		
		if(e.getSource() == multi_label){
			
			try {
				new Tetris_multi_frame();
			} catch (BindException e1) {
				
				System.out.println("이미 서버가 실행 중입니다.");
			}
			
		}
		
		if(e.getSource() == exit_label){
			
			System.exit(1);
			
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {

		setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		if(e.getSource() == single_label)
			single_label.setFont(new Font("",Font.BOLD,20));
		
		if(e.getSource() == multi_label)
			multi_label.setFont(new Font("",Font.BOLD,20));
		
		if(e.getSource() == exit_label)
			exit_label.setFont(new Font("",Font.BOLD,20));
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		
		if(e.getSource() == single_label)
			single_label.setFont(new Font("",Font.PLAIN,20));
		
		if(e.getSource() == multi_label)
			multi_label.setFont(new Font("",Font.PLAIN,20));
		
		if(e.getSource() == exit_label)
			exit_label.setFont(new Font("",Font.PLAIN,20));
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		
		new Tetris_main();

	}

	

}
